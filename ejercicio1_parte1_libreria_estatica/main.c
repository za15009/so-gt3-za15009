/*
ARCHIVO: Ejercicio1
Juan Carlos Zepeda Abrego
*/

#include <math.h>
#include "libstop.h"

int opcion = 1, numero;

int main(){

	printf("Seleccione el programa que desea ejecutar\n");
	printf("1. Cubo: Eleva un numero entero al cubo\n");
	printf("2. Factorial: Encuentra el factorial de un numero entero\n");
	printf("0. Salir\n");
	scanf("%d", &opcion);

	switch(opcion){

	case 1:
	system("clear");
	printf("Introduzca el número entero para elevarlo al Cubo\t");
	scanf("%d", &numero); 
	cubo(numero);
	break;

	case 2:
	system("clear");
	printf("Introduzca el número entero para sacar su Factorial:\t");
	scanf("%d", &numero);
	factorial(numero);
	}

return 0;
}
