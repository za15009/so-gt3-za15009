/*
* ARCHIVO: libstop.h
* DESCRIPCION: Cabecera de dependencias
* Juan Carlos Zepeda Abrego
*/

#ifndef LIBSTOP_H
#define LIBSTOP_H

#include<stdio.h>
#include<stdlib.h>
int cubo(int);
int factorial(int);
#endif
