.section .data
numUno: .long 0
numDos: .long 0
introValorUno: .ascii "Introducir el primer número a sumar\0",
introValorDos: .ascii "Introducir el segundo número a sumar\0",
resultado: .ascii "La suma es: %d\n"
tipo: .ascii "%d"

.section .bss

.section .text

.globl main

main:
	pushl	$introValorUno
	call	printf
	addl	$4,	%esp
	pushl	$numUno
	pushl	$tipo
	call	scanf
	addl	$4,	%esp

	pushl	$introValorDos
	call	printf
	addl	$4,	%esp
	pushl	$numDos
	pushl	$tipo
	call	scanf
	addl	$8,	%esp

	movl	numUno,	%eax
	addl	numDos,	%eax
	pushl		%eax
	pushl	$resultado
	call	printf
	addl	$4,	%esp
	movl	$0,	%ebx
	movl	$1,	%eax
	int	$0x80
