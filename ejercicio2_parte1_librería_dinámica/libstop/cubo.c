/*
* ARCHIVO: cubo.c
* DESCRIPCION: Devuelve el cubo de un numero entero
* Juan Carlos Zepeda Abrego
*/

#include <stdio.h>
#include <math.h>

int cubo(int base) {
	int resultado; 
	int exponente=3;
	resultado = pow(base, exponente);
	printf("El cubo del número es: \t");
	printf("%d",resultado);
	return resultado;
}
