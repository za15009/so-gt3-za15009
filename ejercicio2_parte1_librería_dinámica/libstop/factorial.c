#include <stdio.h>
#include <stdlib.h>

int factorial(int numero){
	int resultado;

	resultado = 1;
	while(numero > 1) {
		resultado *= numero;
		numero --;
	}
	printf("El factorial del número es: \t");
	printf("%d",resultado);
	return resultado;
}
